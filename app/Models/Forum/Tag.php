<?php

namespace App\Models\Forum;

use App\Traits\HasManyThread;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasManyThread;
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
