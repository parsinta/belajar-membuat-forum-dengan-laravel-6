<?php 
namespace App\Traits;

use App\Models\Forum\Reply;

trait HasManyReply
{
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }
}
