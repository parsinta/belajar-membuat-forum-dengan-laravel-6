<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        return view('account.edit');
    }

    public function update()
    {
        $avatar = request()->file('avatar');
        $avatar_validate = 'image|mimes:jpeg,png,jpg,svg|max:2048';

        request()->validate([
            'username' => 'required|alpha_num|min:3|max:20|unique:users,username,' . auth()->id(),
            'name' => 'string|required',
            'avatar' => $avatar ? $avatar_validate : "",
        ]);

        $hash = auth()->user()->hash;

        $avatar_name = $avatar->storeAs('profile-picture', "{$hash}.{$avatar->extension()}");

        auth()->user()->update([
            'username' => request('username'),
            'name' => request('name'),
            'avatar' => $avatar_name,
        ]);

        return redirect()->route('users.show', auth()->user()->usernameOrHash());
    }
}
