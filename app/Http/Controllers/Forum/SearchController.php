<?php

namespace App\Http\Controllers\Forum;

use App\Models\Forum\Thread;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function index()
    {
        $query = request('query');
        $threads = Thread::search($query)->paginate(10);
        return view('threads.search', compact('threads', 'query'));
    }
}
