@auth
    <div class="media mt-3">
        <img width="40" height="40" src="{{ asset(auth()->user()->avatar()) }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
        <div class="media-body">
            <div class="mb-1 text-secondary">
                <strong>{{ auth()->user()->name }}</strong>
            </div>
            <div class="text-secondary">
                <form action="{{ route('replies.create', $thread) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <textarea name="body" id="body" rows="5" class="form-control" style="resize: none;" placeholder="Submit a comment..."></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Reply</button>
                </form>
            </div>
        </div>
    </div>
@else
    <div class="mt-2 text-secondary">
        Plaase <a href="{{ route('login') }}">Login</a> to reply a thread.
    </div>
@endauth
