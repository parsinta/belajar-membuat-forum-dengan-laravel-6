<!-- Button trigger modal -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{ $thread->slug }}">
  Delete
</button>

<!-- Modal -->
<div class="modal fade" id="{{ $thread->slug }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-inline">
          <li class="list-inline-item">
            <form action="{{ route('threads.delete', $thread) }}" method="post">
              @csrf
              @method("DELETE")
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>
          </li>
          <li class="list-inline-item">
            <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
