@csrf
<div class="form-group">
    <label for="title">Title</label>
    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') ?? $thread->title }}">
    {!! $errors->first('title', '<div class="text-danger mt-1">:message</div>') !!}
</div>

<div class="form-group">
    <label for="tag">Tag</label>
    <select name="tag" id="tag" class="form-control">
        <option disabled selected>Choose one</option>

        @foreach ($tags as $tag)
            <option {{ $tag->id == $thread->tag_id ? 'selected' : '' }} value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>
    {!! $errors->first('tag', '<div class="text-danger mt-1">:message</div>') !!}
</div>

<div class="form-group">
    <label for="body">Body</label>
    <textarea name="body" id="body" rows="10" class="form-control">{{ old('body') ?? $thread->body }}</textarea>
    {!! $errors->first('body', '<div class="text-danger mt-1">:message</div>') !!}
</div>

<button type="submit" class="btn btn-primary">{{ $submit }}</button>


