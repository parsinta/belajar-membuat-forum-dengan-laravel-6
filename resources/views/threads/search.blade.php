@extends('layouts.app')
@section('title', 'Parsinta')
@section('content')
    <form action="{{ route('threads.search') }}" method="get" class="mb-2" autocomplete="off">
        <input type="search" name="query" class="form-control" placeholder="Search threads . . ." value="{{ $query }}" autofocus>
    </form>
    <div class="card">
        <div class="card-header">
            <a href="{{ route('threads') }}" class="text-secondary">Forum</a>
            @isset ($tag)
                <span class="text-secondary">
                    / {{ $tag->name }}
                </span>
            @endisset
        </div>

        <div class="card-body">
            @forelse ($threads as $thread)
                <div class="row mb-4 d-flex align-items-center">
                    <div class="col-md-10">
                        <div class="media">
                            <img width="40" height="40" src="{{ $thread->user->avatar() }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
                            <div class="media-body">
                                <a href="{{ route('threads.show', [$thread->tag, $thread]) }}" class="mt-0 d-block">{{ $thread->title }}</a>
                                <div class="text-secondary">
                                    {{ Str::limit($thread->body, 120) }}
                                </div>
                                <small class="text-secondary">
                                    <a href="#" class="text-secondary">{{ $thread->user->name }}</a> posted {{ $thread->created_at->diffForHumans() }}
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <small>
                            <a href="{{ route('tags.show', $thread->tag) }}" class="text-secondary">{{ $thread->tag->name }}</a>
                        </small>
                    </div>
                </div>
            @empty
                We can not find what are you looking for ... <a href="{{ route('threads') }}">Browse threads</a>.
            @endforelse

            {{ $threads->appends($query)->links() }}
        </div>
    </div>
@endsection
