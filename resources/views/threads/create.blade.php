@extends('layouts.app')
@section('title', 'Create new thread')
@section('content')
    <div class="card">
        <div class="card-header">New thread</div>

        <div class="card-body">
            <form action="{{ route('threads.create') }}" method="post">
                @include('threads.partials.form', ['submit' => 'Create'])
            </form>
        </div>
    </div>
@endsection
