@extends('layouts.app')
@section('title')
    Edit thread: {{ $thread->title }}
@endsection
@section('content')
    <div class="card">
        <div class="card-header">Edit thread: {{ $thread->title }}</div>

        <div class="card-body">
            <form action="{{ route('threads.edit', [$thread->tag, $thread]) }}" method="post" class="mb-2">
                @method("PATCH")
                @include('threads.partials.form', ['submit' => 'Update'])
            </form>

            @include('threads.delete')
        </div>
    </div>
@endsection
