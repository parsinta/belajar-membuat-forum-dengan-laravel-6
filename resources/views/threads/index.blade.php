@extends('layouts.app')
@section('title', 'Parsinta')
@section('content')
    <form action="{{ route('threads.search') }}" method="get" class="mb-2" autocomplete="off">
        <input type="search" name="query" class="form-control" placeholder="Search threads . . .">
    </form>
    <div class="card">
        <div class="card-header">
            <a href="{{ route('threads') }}" class="text-secondary">Forum</a>
            @isset ($tag)
                <span class="text-secondary">
                    / {{ $tag->name }}
                </span>
            @endisset
        </div>

        <div class="card-body">
            @foreach ($threads as $thread)
                <div class="row mb-4 d-flex align-items-center">
                    <div class="col-md-10">
                        <div class="media">
                            <img width="40" height="40" src="{{ asset($thread->user->avatar()) }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
                            <div class="media-body">
                                <a href="{{ route('threads.show', [$thread->tag, $thread]) }}" class="mt-0 d-block">{{ $thread->title }}</a>
                                <div class="text-secondary">
                                    {{ Str::limit($thread->body, 120) }}
                                </div>
                                <small class="text-secondary">
                                    <a href="{{ route('users.show', $thread->user->usernameOrHash()) }}" class="text-secondary">{{ $thread->user->name }}</a> posted {{ $thread->created_at->diffForHumans() }}
                                    &middot; {{ $thread->replies_count }} {{ Str::plural('reply', $thread->replies_count) }}
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <small>
                            <a href="{{ route('tags.show', $thread->tag) }}" class="text-secondary">{{ $thread->tag->name }}</a>
                        </small>
                    </div>
                </div>
            @endforeach
            @isset ($query)
                {{ $threads->appends(['by' => $query])->links() }}
            @else
                {{ $threads->links() }}
            @endisset
        </div>
    </div>
@endsection
