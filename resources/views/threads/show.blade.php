@extends('layouts.app')
@section('title', $thread->title)
@section('content')
    <div class="card{{ count($replies) >= 1 ? ' pb-3' : '' }}">
        <div class="card-header">{{ $thread->title }}</div>
        <div class="card-body">
            <div class="media">
                <img width="40" height="40" src="{{ asset($thread->user->avatar()) }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
                <div class="media-body">
                    <a href="{{ route('users.show', $thread->user->usernameOrHash()) }}" class="mt-0 d-block">{{ $thread->user->name }}</a>
                    <div class="text-secondary">
                        {!! nl2br($thread->body) !!}
                    </div>
                    <small class="text-secondary">
                        Posted {{ $thread->created_at->diffForHumans() }} &middot; <a href="{{ route('tags.show', $thread->tag) }}" class="text-secondary">{{ $thread->tag->name }}</a>

                        @can('update', $thread)
                            &middot; <a href="{{ route('threads.edit', [$thread->tag, $thread]) }}">Edit</a>
                        @endcan
                    </small>
                </div>
            </div>
        </div>
    </div>

    @if ($thread->answer)
        <div class="row justify-content-end mr-3 mb-5" style="margin-top: -2em">
            <div class="col-md-11">
                <div class="card">
                    <div class="card-header">Best Answer</div>
                    <div class="card-body">
                        <div class="media mb-3">
                            <img width="40" height="40" src="{{ $thread->answer->user->avatar() }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
                            <div class="media-body">
                                <a href="#" class="mt-0 d-block">{{ $thread->answer->user->name }}</a>
                                <div class="text-secondary">
                                    {!! $thread->answer->body !!}
                                </div>
                                <small class="text-secondary d-flex justify-content-between">
                                    <div>
                                        Answered at {{ $thread->answer->created_at->diffForHumans() }}
                                    </div>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    
    
    @include('replies.index')
    @include('replies.create')
@endsection
