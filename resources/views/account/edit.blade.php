@extends('layouts.app')
@section('title', 'Edit Profile')
@section('content')
    <div class="card">
        <div class="card-header">Edit Profile</div>
            <div class="card-body">
                <form action="{{ route('account.edit') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method("PATCH")
                    <div class="form-group">
                        <label for="avatar">Upload avatar</label>
                        <input type="file" name="avatar" id="avatar" class="form-control-file">
                        {!! $errors->first('avatar', '<div class="text-danger mt-1">:message</div>') !!}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="@johndoe" value="{{ old('username') ?? auth()->user()->username }}">
                                {!! $errors->first('username', '<div class="text-danger mt-1">:message</div>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ?? auth()->user()->name }}">
                                {!! $errors->first('name', '<div class="text-danger mt-1">:message</div>') !!}
                            </div>
                        </div>
                    </div> <!-- row -->
                    <button type="submit" class="btn btn-secondary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
