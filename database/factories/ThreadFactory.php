<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Forum\Thread;
use Faker\Generator as Faker;

$factory->define(Thread::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'slug' => \Str::slug($faker->sentence),
        'user_id' => rand(1, 2),
        'tag_id' => rand(1, 3),
        'body' => $faker->paragraph(15),
    ];
});
